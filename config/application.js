var Config;

Config = {
  apiPath: '@@apiPath',
  rootPath: '@@rootPath',
  headers: {
    gisTokenId: '@@gisToken',
    userTokenId: '@@userToken'
  }
};

module.exports = Config;