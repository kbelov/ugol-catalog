'use strict';

var $ = require('jquery'),
    Isotope = require('isotope-layout');

document.addEventListener('DOMContentLoaded', function () {
    var $grid = $('.grid');

    function updateItemsHeight() {
        $grid.removeClass('grid--layout-complete');

        var $items = $('.catalog-item');

        var max_size = 0;

        $.each($items, function (index, item) {
            $(this).css('height', 'auto');

            var height = $(this).outerHeight();

            console.log(height);
            if (max_size < height) {
                max_size = height;
            }
        });

        $items.css('height', max_size);

        $grid.addClass('grid--layout-complete');
    }

    updateItemsHeight();

    var grid = new Isotope('.grid', {
        itemSelector: '.grid-item',
        getSortData: {
            status: '.catalog-item-status-value',
            date: function(itemElem) {
                var date = $(itemElem).find('.catalog-item-date-value').text(),
                    dateArray = date.split('.'),
                    year = dateArray[2],
                    month = dateArray[1],
                    day = dateArray[0];

                return new Date(year, month, day);
            }
        }
    });

    grid.on( 'layoutComplete', function( event, laidOutItems ) {
        setTimeout(function () {
            updateItemsHeight();
        }, 100)
    });

    $('.sort-by-date').on('click', function () {
        grid.arrange({ sortBy: 'date'});
    });

    $('.sort-by-status').on('click', function () {
        grid.arrange({ sortBy: 'status'});
    });
});