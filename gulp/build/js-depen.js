import gulp from "gulp";
import plumber from "gulp-plumber";
import rename from "gulp-rename";
import config from "../config";

gulp.task('js-depen', () => {

    var ol = '';

    if (config.development) {
        ol = 'node_modules/openlayers/dist/ol-debug.js';
    } else {
        ol = 'node_modules/openlayers/dist/ol.js';
        // ol = 'app/js/libs/ol.compile.js';
    }

    return gulp.src(ol)
        .pipe(plumber())
        .pipe(rename('ol.js'))
        .pipe(gulp.dest(config.distDir + '/js'));
});
