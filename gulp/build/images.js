import gulp from "gulp";
import plumber from "gulp-plumber";
import imagemin from "gulp-imagemin";
import config from "../config";

gulp.task('images', () => {
  return gulp.src([
    `${config.appDir}/res/**/*`
  ], { base: config.appDir })
    .pipe(plumber())
    .pipe(imagemin({
      progressive: true
    }))
    .pipe(gulp.dest(config.distDir));
});
