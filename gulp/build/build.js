import gulp from "gulp";
import runSequence from "run-sequence";

gulp.task('build', (callback) => {
  runSequence(
    'clean',
    [
      'fonts',
      'images',
      'js-depen',
      'templates',
      'stylesheets',
      'replace'
    ],
    'browserify',
    callback
  );
});
